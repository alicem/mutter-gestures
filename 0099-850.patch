From 6db1440c48b5eb1d744ff6b559364777cf9cc0cc Mon Sep 17 00:00:00 2001
From: Alexander Mikhaylenko <exalm7659@gmail.com>
Date: Sat, 12 Oct 2019 23:53:19 +0500
Subject: [PATCH 1/4] stack: Check workspace in get_default_focus_window()

Check against the window argument, instead of active workspace.

https://gitlab.gnome.org/GNOME/mutter/merge_requests/850
---
 src/core/stack.c | 13 +++++++++----
 1 file changed, 9 insertions(+), 4 deletions(-)

diff --git a/src/core/stack.c b/src/core/stack.c
index 1a05de264..75062865e 100644
--- a/src/core/stack.c
+++ b/src/core/stack.c
@@ -33,6 +33,7 @@
 #include "cogl/cogl-trace.h"
 #include "core/frame.h"
 #include "core/meta-workspace-manager-private.h"
+#include "core/workspace-private.h"
 #include "core/window-private.h"
 #include "meta/group.h"
 #include "meta/prefs.h"
@@ -1150,7 +1151,8 @@ window_contains_point (MetaWindow *window,
 }
 
 static gboolean
-window_can_get_default_focus (MetaWindow *window)
+window_can_get_default_focus (MetaWindow    *window,
+                              MetaWorkspace *workspace)
 {
   if (window->unmaps_pending > 0)
     return FALSE;
@@ -1161,7 +1163,10 @@ window_can_get_default_focus (MetaWindow *window)
   if (!meta_window_is_focusable (window))
     return FALSE;
 
-  if (!meta_window_should_be_showing (window))
+  if (!meta_window_showing_on_its_workspace (window))
+    return FALSE;
+
+  if (workspace != NULL && g_list_find (workspace->windows, window) == NULL)
     return FALSE;
 
   if (window->type == META_WINDOW_DOCK)
@@ -1197,7 +1202,7 @@ get_default_focus_window (MetaStack     *stack,
       if (window == not_this_one)
         continue;
 
-      if (!window_can_get_default_focus (window))
+      if (!window_can_get_default_focus (window, workspace))
         continue;
 
       if (must_be_at_point && !window_contains_point (window, root_x, root_y))
@@ -1268,7 +1273,7 @@ meta_stack_get_default_focus_candidates (MetaStack     *stack,
     {
       GList *next = l->next;
 
-      if (!window_can_get_default_focus (l->data))
+      if (!window_can_get_default_focus (l->data, workspace))
         windows = g_list_delete_link (windows, l);
 
       l = next;
-- 
2.23.0

From 1211aa4f14fe3008cea63af876328c703662d60e Mon Sep 17 00:00:00 2001
From: Alexander Mikhaylenko <exalm7659@gmail.com>
Date: Sat, 12 Oct 2019 23:55:01 +0500
Subject: [PATCH 2/4] workspace: Add meta_workspace_get_default_focus_window()

This will be used in the next commit to make this window appear focused.

https://gitlab.gnome.org/GNOME/mutter/merge_requests/850
---
 src/core/workspace-private.h | 1 +
 src/core/workspace.c         | 9 +++++++++
 2 files changed, 10 insertions(+)

diff --git a/src/core/workspace-private.h b/src/core/workspace-private.h
index a58b2347d..2f78f2e25 100644
--- a/src/core/workspace-private.h
+++ b/src/core/workspace-private.h
@@ -92,6 +92,7 @@ GList* meta_workspace_get_onscreen_region       (MetaWorkspace *workspace);
 GList * meta_workspace_get_onmonitor_region (MetaWorkspace      *workspace,
                                              MetaLogicalMonitor *logical_monitor);
 
+MetaWindow* meta_workspace_get_default_focus_window (MetaWorkspace *workspace);
 void meta_workspace_focus_default_window (MetaWorkspace *workspace,
                                           MetaWindow    *not_this_one,
                                           guint32        timestamp);
diff --git a/src/core/workspace.c b/src/core/workspace.c
index 59a502df1..3ed91b58c 100644
--- a/src/core/workspace.c
+++ b/src/core/workspace.c
@@ -1268,6 +1268,15 @@ meta_workspace_get_name (MetaWorkspace *workspace)
   return meta_prefs_get_workspace_name (meta_workspace_index (workspace));
 }
 
+MetaWindow *
+meta_workspace_get_default_focus_window (MetaWorkspace *workspace)
+{
+  if (meta_prefs_get_focus_mode () == G_DESKTOP_FOCUS_MODE_CLICK)
+    return meta_stack_get_default_focus_window (workspace->display->stack, workspace, NULL);
+
+  return NULL;
+}
+
 void
 meta_workspace_focus_default_window (MetaWorkspace *workspace,
                                      MetaWindow    *not_this_one,
-- 
2.23.0

From 110542fc9a5f19693db9a7e371b6cab889c9acff Mon Sep 17 00:00:00 2001
From: Alexander Mikhaylenko <exalm7659@gmail.com>
Date: Sat, 12 Oct 2019 23:55:26 +0500
Subject: [PATCH 3/4] window: Make default focus window on each workspace
 appear focused

Makes workspace transitions in gnome-shell look more seamless, since
both outgoing and incoming workspace have focused windows.

This is only done for click focus mode, since it's not known which
window would be focused for the other modes.

https://gitlab.gnome.org/GNOME/mutter/merge_requests/850
---
 src/core/window-private.h |  2 ++
 src/core/window.c         | 21 +++++++++++++++++++--
 src/core/workspace.c      | 25 +++++++++++++++++++++++++
 3 files changed, 46 insertions(+), 2 deletions(-)

diff --git a/src/core/window-private.h b/src/core/window-private.h
index dd89fdc90..4715c6692 100644
--- a/src/core/window-private.h
+++ b/src/core/window-private.h
@@ -680,6 +680,8 @@ void        meta_window_get_session_geometry (MetaWindow  *window,
 
 void        meta_window_update_unfocused_button_grabs (MetaWindow *window);
 
+void        meta_window_appears_focused_changed (MetaWindow *window);
+
 void     meta_window_set_focused_internal (MetaWindow *window,
                                            gboolean    focused);
 
diff --git a/src/core/window.c b/src/core/window.c
index e276b1e59..7f04bf7d2 100644
--- a/src/core/window.c
+++ b/src/core/window.c
@@ -251,6 +251,10 @@ prefs_changed_callback (MetaPreference pref,
       meta_window_recalc_features (window);
       meta_window_queue (window, META_QUEUE_MOVE_RESIZE);
     }
+  else if (pref == META_PREF_FOCUS_MODE)
+    {
+      meta_window_appears_focused_changed (window);
+    }
 }
 
 static void
@@ -4860,6 +4864,9 @@ set_workspace_state (MetaWindow    *window,
         }
     }
 
+  if (!window->constructing)
+    meta_window_appears_focused_changed (window);
+
   /* queue a move_resize since changing workspaces may change
    * the relevant struts
    */
@@ -5159,7 +5166,7 @@ meta_window_change_workspace_by_index (MetaWindow *window,
     meta_window_change_workspace (window, workspace);
 }
 
-static void
+void
 meta_window_appears_focused_changed (MetaWindow *window)
 {
   set_net_wm_state (window);
@@ -7234,7 +7241,17 @@ meta_window_get_frame (MetaWindow *window)
 gboolean
 meta_window_appears_focused (MetaWindow *window)
 {
-  return window->has_focus || (window->attached_focus_window != NULL);
+  MetaWorkspaceManager *workspace_manager;
+  MetaWorkspace *workspace;
+  MetaWindow *default_window = NULL;
+
+  workspace_manager = window->display->workspace_manager;
+  workspace = meta_window_get_workspace (window);
+
+  if (workspace && workspace != workspace_manager->active_workspace)
+    default_window = meta_workspace_get_default_focus_window (workspace);
+
+  return window->has_focus || (window->attached_focus_window != NULL) || (window == default_window);
 }
 
 gboolean
diff --git a/src/core/workspace.c b/src/core/workspace.c
index 3ed91b58c..85cb3f223 100644
--- a/src/core/workspace.c
+++ b/src/core/workspace.c
@@ -356,10 +356,26 @@ meta_workspace_remove (MetaWorkspace *workspace)
    */
 }
 
+static void
+update_default_focus (MetaWorkspace *workspace,
+                      MetaWindow    *not_this_one)
+{
+  GList *l;
+
+  for (l = workspace->windows; l; l = l->next) {
+    MetaWindow *window = META_WINDOW (l->data);
+
+    if (window != not_this_one)
+      meta_window_appears_focused_changed (window);
+  }
+}
+
 void
 meta_workspace_add_window (MetaWorkspace *workspace,
                            MetaWindow    *window)
 {
+  MetaWorkspaceManager *manager = workspace->display->workspace_manager;
+
   COGL_TRACE_BEGIN_SCOPED (MetaWorkspaceAddWindow,
                            "Workspace (add window)");
 
@@ -376,6 +392,9 @@ meta_workspace_add_window (MetaWorkspace *workspace,
       meta_workspace_invalidate_work_area (workspace);
     }
 
+  if (workspace != manager->active_workspace)
+    update_default_focus (workspace, window);
+
   g_signal_emit (workspace, signals[WINDOW_ADDED], 0, window);
   g_object_notify_by_pspec (G_OBJECT (workspace), obj_props[PROP_N_WINDOWS]);
 }
@@ -384,6 +403,8 @@ void
 meta_workspace_remove_window (MetaWorkspace *workspace,
                               MetaWindow    *window)
 {
+  MetaWorkspaceManager *manager = workspace->display->workspace_manager;
+
   COGL_TRACE_BEGIN_SCOPED (MetaWorkspaceRemoveWindow,
                            "Workspace (remove window)");
 
@@ -400,6 +421,9 @@ meta_workspace_remove_window (MetaWorkspace *workspace,
       meta_workspace_invalidate_work_area (workspace);
     }
 
+  if (workspace != manager->active_workspace)
+    update_default_focus (workspace, NULL);
+
   g_signal_emit (workspace, signals[WINDOW_REMOVED], 0, window);
   g_object_notify (G_OBJECT (workspace), "n-windows");
 }
@@ -644,6 +668,7 @@ meta_workspace_activate_with_focus (MetaWorkspace *workspace,
   if (focus_this)
     {
       meta_window_activate (focus_this, timestamp);
+      update_default_focus (workspace, focus_this);
     }
   else if (move_window)
     {
-- 
2.23.0

From ac08cd8f342d4aa23a9d52b01e2a2b5fe722ac82 Mon Sep 17 00:00:00 2001
From: Alexander Mikhaylenko <exalm7659@gmail.com>
Date: Sun, 13 Oct 2019 05:15:51 +0500
Subject: [PATCH 4/4] window: Appear as focused while focusing

Prevent focused window from blinking after switching to another
workspace on X11.

https://gitlab.gnome.org/GNOME/mutter/merge_requests/850
---
 src/core/window-private.h | 3 +++
 src/core/window.c         | 9 ++++++++-
 2 files changed, 11 insertions(+), 1 deletion(-)

diff --git a/src/core/window-private.h b/src/core/window-private.h
index 4715c6692..4f3329bd2 100644
--- a/src/core/window-private.h
+++ b/src/core/window-private.h
@@ -540,6 +540,9 @@ struct _MetaWindow
   int constrained_placement_rule_offset_y;
 
   guint unmanage_idle_id;
+
+  /* Prevent blinking when focusing */
+  gboolean focusing;
 };
 
 struct _MetaWindowClass
diff --git a/src/core/window.c b/src/core/window.c
index 7f04bf7d2..64f8b8714 100644
--- a/src/core/window.c
+++ b/src/core/window.c
@@ -4773,6 +4773,10 @@ meta_window_focus (MetaWindow  *window,
       window = modal_transient;
     }
 
+  /* If the window was already appearing focused, but didn't have has_focus set,
+   * it will now briefly appear unfocused on X11. Set a flag to prevent that. */
+  window->focusing = TRUE;
+
   meta_window_flush_calc_showing (window);
 
   if ((!window->mapped || window->hidden) && !window->shaded)
@@ -4780,6 +4784,7 @@ meta_window_focus (MetaWindow  *window,
       meta_topic (META_DEBUG_FOCUS,
                   "Window %s is not showing, not focusing after all\n",
                   window->desc);
+      window->focusing = FALSE;
       return;
     }
 
@@ -5262,6 +5267,8 @@ meta_window_set_focused_internal (MetaWindow *window,
 {
   MetaWorkspaceManager *workspace_manager = window->display->workspace_manager;
 
+  window->focusing = FALSE;
+
   if (focused)
     {
       window->has_focus = TRUE;
@@ -7251,7 +7258,7 @@ meta_window_appears_focused (MetaWindow *window)
   if (workspace && workspace != workspace_manager->active_workspace)
     default_window = meta_workspace_get_default_focus_window (workspace);
 
-  return window->has_focus || (window->attached_focus_window != NULL) || (window == default_window);
+  return window->has_focus || window->focusing || (window->attached_focus_window != NULL) || (window == default_window);
 }
 
 gboolean
-- 
2.23.0

